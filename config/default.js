'use strict';

const port = process.env.PORT || 8080;

module.exports = {
    server: {
        port: port
    },
    log: {
        level: "Dev",
        http: ':remote-addr [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer"'
    }
};