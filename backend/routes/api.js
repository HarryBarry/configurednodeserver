'use strict';

const express = require('express');
const dem = express.Router();

dem.get('/', (req, res) => {
    res.render('index', {title: 'Test'})
});

module.exports = dem;
