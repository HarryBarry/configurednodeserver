'use strict';

const helmet = require('helmet');
const session = require('cookie-session');
const compression = require('compression');
const log = require('npmlog');
const config = require('config');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const favicon = require('express-favicon');
const logger = require('morgan');

const LOKI = express();
const server = require('http').Server(LOKI);
const dev = (process.argv[2] === '--dev');
const api = require('./backend/routes/api');

log.level = config.log.level;

LOKI.use(helmet.hidePoweredBy({setTo: 'LOKI incs'}));
LOKI.use(helmet.noSniff());
LOKI.use(helmet.ieNoOpen());
LOKI.use(helmet.dnsPrefetchControl({allow: false}));

function logErrors(err, req, res, next) {
    console.error(err.stack);
    next(err);
}

function clientErrorHandler(err, req, res, next) {
    if (req.xhr) {
        res.status(500).send({error: 'Something failed!'});
    } else {
        next(err);
    }
}

function errorHandler(err, req, res, next) {
    res.status(500);
    res.render('error', {error: err});
}

let allowCrossDomain = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

LOKI.use(
  require('morgan')(
    config.log.http, {
      'stream': {
        'write': (line) => {
          if ((line = (line || '').trim())) {
            log.http('loki:server', line);
          }
        }
      }
    }
  )
);

// view engine setup
LOKI.set('views', path.join(__dirname, 'template'));
LOKI.set('view engine', 'jade');
// LOKI.use(favicon(path.join(__dirname, 'static', 'favicon.ico')));
LOKI.use(logger('dev'));
LOKI.use(bodyParser.json());
LOKI.use(bodyParser.urlencoded({ extended: true }));
LOKI.use(cookieParser('Material_Secret_Key'));
LOKI.use(express.static(path.join(__dirname, 'static'), {maxAge: 864000000}));
LOKI.use(allowCrossDomain);
LOKI.use(logErrors);
LOKI.use(clientErrorHandler);
LOKI.use(errorHandler);
LOKI.use(compression());


LOKI.use('/', api);


// catch 404 and forward to errors handler
LOKI.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// errors handlers


LOKI.use(
    (err, req, res, next) => {
      console.error(err.stack);
      res.status(500).json({response: "500"});
    }
);


// production errors handler
// no stacktraces leaked to user
LOKI.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

server.listen(config.server.port);

if (dev) {
    console.log(' > starting in development mode');
}
console.log(' > listening on http://localhost:' + config.server.port + '\n');

module.exports = LOKI;
